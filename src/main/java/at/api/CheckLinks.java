package at.api;


import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class CheckLinks {
    public static void checkLink(){
        Integer pageNumber;
        for (pageNumber = 1; pageNumber < 10000; pageNumber++) {
            System.out.println(pageNumber);
            Response response;
            response = given().
                    contentType(ContentType.ANY).
                    when().
                    get("https://s3.eu-central-1.amazonaws.com/qa-web-test-task/"+pageNumber+".html");
            response.then().
                    statusCode(200).
                    body(containsString("href")).
        log().all();

        }
    }
}
